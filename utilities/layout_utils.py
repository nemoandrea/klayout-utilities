import pya  # import KLayout core
import os 
import operator
from enum import Enum
from functools import partial

def fileinfo(layout):
  '''Simple debug function that prints cells and layers in layout'''
  for cell in layout.each_cell():
    print(f"cell name: {cell.name}")

  for layer_idx in range(layout.layers()):
    print(f"layer index {layer_idx} has name: {layout.get_info(layer_idx)}")


def load_template(output, template_path, template_cell_name="TEMPLATE", flatten=False):
    """
    Load a an external file as template to use in a final output file/layout
    
    The template file design will be loaded into the template cell with name
    'template_cell_name', where the orignal layer names are retained 
    """
    template_layout = pya.Layout()
    template_layout.read(template_path)

    if flatten:
      flatten_layers(template_layout)

    # TODO: Assert output cell name exists

    # we assume designs live in "TOP" cell. Is True for FreeCAD exports
    output.cell(template_cell_name).copy_tree(template_layout.cell("TOP"))  
    template_layout._destroy()  # free resources


def setup_output_file(template_files):
  '''template_files must be dict'''

  assert type(template_files) is dict, "template files must be a dict of (filename, cellname)"
  
  # ensure we are sorted by value, so that items are squentially grouped by cellname (python 3.7+)
  template_files = {k: v for k, v in sorted(template_files.items(), key=lambda x: x[1])} 

  # create an output layout (i.e. document)
  output = pya.Layout()
  output.create_cell("TOP")

  for (filepath, cellname) in template_files.items():
    # check if cell already exists, and if not, add it
    if not (cellname in [cell.name for cell in output.each_cell()]):
      output.create_cell(cellname)
    # add the file to the current cell
    load_template(output, filepath, cellname)  # e.g. "../device/microchannels.dxf" and "TEMPLATE"

  return output


def place_design(output, cellname, x, y): 
    '''Syntax sugar for placing a copy of a cell in the TOP cell at location specified in x and y'''
    output.cell("TOP").insert(pya.DCellInstArray(output.cell(cellname).cell_index(), pya.DTrans(pya.DVector(x, y)), pya.DVector(), pya.DVector(), 1, 0))


def place_text(output, text, size, x, y, layer_idx=0):
    """size is stroke size in um"""
    # see https://www.klayout.de/staging/doc/code/class_TextGenerator.html
    # text (string text, double target_dbu, double mag = 1, bool inv = false, double bias = 0, double char_spacing = 0, double line_spacing = 0)
    txt_region = pya.TextGenerator.default_generator().text(text, 0.0001, size, False, 0,0,0)

    scalefactor =1000 # no idea why this is needed
    txt = txt_region.transformed(pya.DTrans(pya.DVector(x*scalefactor, y*scalefactor))) # move text

    output.cell("TOP").shapes(layer_idx).insert(txt)

# See operator module for more options: https://docs.python.org/3/library/operator.html#mapping-operators-to-functions
class BOOLMODE(Enum):
    '''Boolean operators available in KLayout
    
    Note: I have not tested if more logical operators are available, but these are accessible
    through the gui.'''
    BEGIN = True  # only for the first file of combine_dxf_files(), where you cannot run boolean logic 
    OR = partial(operator.or_)   
    AND = partial(operator.and_)
    A_NOT_B = partial(operator.sub)  # note that B_NOT_A can be achieved be reversing sequence of operations
    XOR= partial(operator.xor)


def combine_dxf_files(files, layername = ""):
    '''
    Performa a series of boolean operations on the dict of filepaths:operation. Returns a new layout
    with the result of sequential boolean operations.

    Note that the values of the `files` dictionary contain the boolean operation to carry out.
    The operation is always carried out on two arguments (i.e. func(a,b)) where a is the result
    of all prior boolean operations and the second is the layout specified as the key in `files`.
    As an example, to boolean subtract a file (B) from another file (A), you would first load file 
    A and then add the dictionary entry `file_path_B:BOOLMODE.A_NOT_B`.
    '''
    assert type(files) is dict, "filepaths must be a dict of (valid dxf file paths, BOOLMODE enum)"

    output = pya.Layout()
    top_cell = output.create_cell("TOP")

    # insert the files to be booled into their own cell and layer

    for i, (filepath, operation) in enumerate(files.items()):
        cellname = os.path.splitext(os.path.basename(filepath))[0]
        
        # move data into the output document in new cell
        output.create_cell(cellname)

        # load the file to run boolean operations on and flatten it (make sure it has 1 layer)
        template_layout = pya.Layout()
        template_layout.read(filepath)
        flatten_layers(template_layout, layername=cellname, design_layer=i)

        # actually copy the design from the "template" to the "output" file
        output.cell(cellname).copy_tree(template_layout.cell("TOP"))  
        template_layout._destroy()  # free resources

        # ensure the new cell is a child of the top cell
        inst = pya.DCellInstArray(output.cell(cellname).cell_index(), pya.DTrans(pya.DVector(0, 0)))
        #inst = pya.DCellInstArray(output.top_cell(), pya.DTrans(pya.DVector(0, 0)))
        top_cell.insert(inst)

        # at this point we have a file with many cells and many layers. We will then run
        # join the files to the 0th layer of the output file by running each file's corresponding
        # boolean operation with the result of the previous operation (stored in the 0th layer)

        # run boolean operations

        if i == 0: # first "layer", by construction that should just be added (no boolean possible)
            # we check the user sent a BOOLMODE.BEGIN argument. It is better to explicitly fail
            # than to be left wondering why the first boolean operation is not doing anything    
            assert operation == BOOLMODE.BEGIN, "The first file to be booled cannot support any \
                boolean operations. Pass the BOOLMODE.BEGIN enum value for this first file."
            
            first_shape = pya.Region(output.top_cell().begin_shapes_rec(0))
            output.top_cell().shapes(0).insert(first_shape)
        else: # and boolean layers together
            r1 = pya.Region(output.top_cell().begin_shapes_rec(0))
            r2 = pya.Region(output.top_cell().begin_shapes_rec(i))
            
            booled = operation.value(r1, r2)  # run boolean operations like OR or A NOT B.
                                              # The .value() is needed due to it being an ENUM val.

            output.clear_layer(0)  # we need to clear the first layer before we paste in our newer version
            
            output.top_cell().shapes(0).insert(booled)
        
    # and finally remove the remaining, non booled layers
    for layer_idx in range(output.layers()-1):
        output.delete_layer(layer_idx+1)

    # rename the layer if a sting is specified
    if layername:
        info = output.get_info(0)
        info.name = layername
        output.set_info(0, info)

    return output


def flatten_layers(layout, layername=False, design_layer=0):
    '''
    Merge all layers into single layer. Removes the other layers.
    
    This is useful for FreeCAD DXF output which often has random empty layers or splits
    output across different layers. design_layer is index of layer where design
    should be saved to. Will also be used with specified layername.
    
    Example 1:
    flatten all layers (boolean OR) and have design end up on layer 0:
    merge_layers_into_one(layout)
    
    Example 2:
    flatten all layers (boolean OR) and have design end up on layer 3 with name "chips":
    merge_layers_into_one(layout, "chips", 3)
    '''

    #print(f"flattening the layers of file with {layout.layers()} layers.")

    # initialise merged to top cell with layer 0. In case the design only has a single layer
    merged = pya.Region(layout.top_cell().begin_shapes_rec(0))

    # merge 1st layer with 2nd layer and paste into first layer. Repeat for 1st and 3rd etc.
    for layer_idx in range(layout.layers()-1):
        r1 = pya.Region(layout.top_cell().begin_shapes_rec(0))
        r2 = pya.Region(layout.top_cell().begin_shapes_rec(layer_idx+1))
        merged = r1 + r2

        layout.clear_layer(0)  # we need to clear the first layer before we paste in our newer version
        
        layout.top_cell().shapes(0).insert(merged)

    # and delete the now pointless 2nd, 3rd, etc layers
    for layer_idx in range(layout.layers()-1):
        layout.delete_layer(layer_idx+1)

    # add back the layer index
    l1 = layout.layer(design_layer,0)
    if design_layer !=0:  # if the desired design layer is not zero we need to move over the design
        layout.top_cell().shapes(l1).insert(merged)
        layout.clear_layer(0)


    # rename the layer if a sting is specified
    if layername:
        info = layout.get_info(l1)
        info.name = layername
        layout.set_info(l1, info)
