# Utility functions for KLayout + FreeCAD workflow

Some utility functions to make creating lithography layouts easier if you use a FreeCAD + KLayout workflow. 

### Motivation

I like to use FreeCAD to design my devices, as a constraint-based workflow is well suited for complex designs. The trouble, however, is when you start to array a complex design onto a wafer - at that point the performance limits of FreeCAD become problematic.

A solution is to make single device designs in FreeCAD and then array them in the excellent KLayout software. 

I find the API documentation of KLayout a bit unclear. For my own sanity I have created this repo which abstracts some of that syntax away and allows a more seamless experience when using the DXF export of FreeCAD. 

### ⚙ Configuring scripts on your system

If you want to run the scripts on your system, you would have to use the following syntax from the command line:

```shell
<path/to/klayout/executable> -e -r <path/to/python/script.py>
```

This is a bit annoying to have to to do, so I suggest the following steps on your system.

1. Alias/add-to-path the KLayout executable.
2. Create a 'clickable' script file that will run the command above.

For windows, you can [add the executable to PATH](https://gist.github.com/ScribbleGhost/752ec213b57eef5f232053e04f9d0d54) and then create a simple `.bat` file with

```bat
ECHO OFF
ECHO Starting KLayout...
klayout_app.exe -e -r klayout-make-wafer.py
```

Which you can then simple doubleclick to run.

## 📃 Functions

### load_template()

Mostly a function for internal use (it is better to use `setup_output_file()`), that adds a template file to an another file, where it can be placed (many times) to make a final output file.

### setup_output_file()

Function that takes a list of files to use as templates, that can later be placed (in the returned layout object) at arbitrary locations with  `place_design()`

Not only is filename provided, but also the cellname for the item to be inserted to. This is useful, because the placement with the command is done on a _per-cell basis. If you want to tile design X, but not design Y (or use a different placement), then you can put them on different cell, and place each design separately.

```python
output = setup_output_file({"design_A.dxf":"EVERYWHERE", 
                            "time-and-date.dxf":"ONCE", 
                           })
for x in range(200):
    place_design(output, "EVERYWHERE", x, 0)
    
place_design(output, "ONCE", 30, 30)  

output.write("layout.dxf")
```

### place_design()

Syntax sugar for the following:

```
output.cell("TOP").insert(pya.DCellInstArray(output.cell(cellname).cell_index(), pya.DTrans(pya.DVector(x, y)), pya.DVector(), pya.DVector(), 1, 0))
```

Which essential places a copy of the content in cell with name `cellname` at location X and Y. The content is inserted into the layer named `TOP`, which is also understood to be the root (topmost) cell of the document. Note that a cell can contain different layers. This function is meant to be used in conjunction with `setup_output_file()` and `load_template()`.

### ENUM BOOLMODE

A simple Enum that captures the GUI boolean operations of KLayout. Contains the following options:

* OR
* AND
* A_NOT_B
* XOR

And the special case

* BEGIN

Which is not a standard option in KLayout, and is only used in `combine_dxf_files`

### combine_dxf_files()

Combine different `.dxf` files into a single layer object with boolean operations. This is useful as FreeCAD is not very good at 2D boolean operations.

Different files are added to a single document in the order specified and with the Boolean operation indicated. For example, one can load file A, Boolean union file B to it and subtract file C from the result. The results are always between the new file, and _the result of all previous operations_. So in the aforementioned example, C is subtracted from (A∪B). The loaded files are flattened (with flatten_layers()) before being combined, so keep that in mind; it could in some cases cause issues. Operations are specified with the enum `BOOLMODE`

The first file **must** start with a `BOOLMODE.BEGIN`, as it (logically) cannot do any Boolean operations.

An optional layername can be provided, and will be assigned to the final resulting structure. Output layout is always single layer.

```python
out = combine_dxf_files({"A.dxf":BOOLMODE.BEGIN,
                         "B.dxf":BOOLMODE.OR,
                         "C.dxf":BOOLMODE.A_NOT_B,
                         }, layername="combined-design")
out.write("output.dxf")
```

### flatten_layers()

Merge all layers into **single** layer, using Boolean union. Removes the other layers. This is useful for FreeCAD DXF output which often has random empty layers or splits output across different layers. `design_layer` is index of layer where design  should be saved to. Will also be used with specified layername.

 ```python
freeCAD_file = pya.Layout()
freeCAD_file.read("freecad-exported-file.dxf")

# Example 1
# flatten all layers (boolean OR) and have design end up on layer 0:
merge_layers_into_one(freeCAD_file)

# Example 2:
# flatten all layers (boolean OR) and have design end up on layer 3 with name "lasercut-shape":
merge_layers_into_one(freeCAD_file, "lasercut-shape", 3)

freeCAD_file.write("single-layer-freeCAD.dxf")
 ```

  

  ## 💡 General Tips

It is quite useful to run the file from the location specified. If running stuff through the KLayout interface you may need

```python
import os
# Change current working directory to the folder the script file is contained in
os.chdir(os.path.dirname(os.path.realpath(__file__)))
```

KLayout scripts can be run through the GUI with the script interface, or directly from terminal  with

```shell
path\to\klayout_app.exe -e -r your-python-script.py
```







